difference () {
    minkowski(){
cube ([25,75,1]);
linear_extrude (1) circle (3);
    }
        
translate ([2,15,-1]) cube ([8,52,5]);
    translate ([15,15,-1]) cube ([8,52,5]);
    
        hull () {
    translate ([5,8,-1]) linear_extrude(3) circle(2);
           translate ([5,8,1]) linear_extrude(3) circle(5);
        }
            
            hull () {
    translate ([19,8,-1]) linear_extrude(3) circle(2);
           translate ([19,8,1]) linear_extrude(3) circle(5);
    }
    }

    
translate ([2,-1.5,2]) linear_extrude (height=1)text ("IgG", size =4.5);
    translate ([16,-1.5,2]) linear_extrude (height=1)text ("IgM", size =4.5);